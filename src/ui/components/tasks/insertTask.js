import React, {Component} from "react";
import axios from 'axios';
import { graphql,gql } from 'react-apollo'
import {Redirect} from "react-router";
class InsertTask extends Component{
    constructor(props){
        super(props);
        this.insertTaskData = this.insertTaskData.bind(this);
        this.state = {
            task: '',
            redirect: false
        }
    }
     gettokenPayload(){
        let token = localStorage.getItem('userToken')
        if(token!==null){
            let payload = token.split('.')[1];
            try {
                payload = JSON.parse(atob(payload))
            }catch(e){
                console.log(e);
                return null
            }
            console.log(payload);
            return payload
        }
        return null
    }
    checkTokenExpiry(){
        let payload = this.gettokenPayload();
        if(payload !== null){
           return payload.iat - new Date().getMilliseconds() < 0 ? true : false;
        }

    }
    componentWillMount(){
        let token = localStorage.getItem('token')
        console.log(token);
        if(token===null || this.checkTokenExpiry()){
            this.setState({redirect:true});
        }

    }

    insertTaskData(e) {
        e.preventDefault();

        var taskName = this.refs.task.value;
        var that = this;
        this.props.addUser({taskName})
            .then(() => {
            console.log("done");

                that.props.history.push('/home')


            })

    }


    render(){
        return (
             this.state.redirect ? <Redirect to="/signin" /> : (
            <form id="insertTaskData" ref="insertTaskData" onSubmit={this.insertTaskData}>
                <input type="text" ref="task" onChange={(e) => this.setState({"task":e.target.value})}/>
                <input type="submit" />
            </form>
            )
        )
    }
}
const addMutation = gql`
  mutation addUser($taskName: String!) {
    addUser(newTodo:{taskName: $taskName}) {
      taskName
    }
  }
`;

//export default InsertTask;
export default graphql(addMutation, {
    props: ({ ownProps, mutate }) => ({
        addUser: ({ taskName }) =>
            mutate({
                variables: { taskName },
            })
    })
})(InsertTask)
