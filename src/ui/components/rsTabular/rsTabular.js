import React,{Component} from "react";
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
const products = [];
import RsTabularConfig from "../../common.js";

class RsTabular extends Component{
    constructor(props){
        super(props);
        //this.getTableHeaders = this.getTableHeaders.bind(this);
    }
    contactFormatter(cell, row){ console.log(cell,row);
        return; }
    getTableHeaders(){
        var tableName = this.props.tableName;
        var index = RsTabularConfig.findIndex(function (d){
            if(d.tableName == tableName){
                return d;
            }
        });
        if(index != -1){
            var config = RsTabularConfig[index].tableConfig;

            return config.map((value,index)=>{
                return (

                    <TableHeaderColumn width='150' dataField={value.data} dataFormat={ this.contactFormatter }  isKey={index == 0 ? true : false}>{value.title}</TableHeaderColumn>
                )
            })
        }
    }


    render(){
        return (

            <BootstrapTable data={ this.props.tableData }>
                {this.getTableHeaders()}
            </BootstrapTable>
        );
    }
}


export default RsTabular;