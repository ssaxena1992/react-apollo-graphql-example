var RsTabularConfig =[
    {
        tableName:"tasks",
        tableConfig:[
            {data:"taskName",title:"Task name"},
            {data:"Author",title:"Author email"}
        ]
    }
];
export default RsTabularConfig;