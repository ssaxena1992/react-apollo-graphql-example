import express from 'express';
import graphqlHTTP from 'express-graphql';
import mongoose from 'mongoose';
import {graphiqlExpress} from "graphql-server-express";
import bodyParser from 'body-parser';
import cors from 'cors';
import { createServer } from 'http';
import { execute, subscribe } from 'graphql';
import { PubSub } from 'graphql-subscriptions';
import { SubscriptionServer } from 'subscriptions-transport-ws';
import { makeExecutableSchema } from 'graphql-tools';
import typeDefs from './schema/schema';
import resolvers from './schema/resolvers';
import { authorizeUser } from './middlewares';
mongoose.connect('mongodb://localhost:27017/react');
const schema = makeExecutableSchema({
    typeDefs,
    resolvers,
});

var port = process.env.API_PORT || 3003;
// const WS_PORT = 5000;
// const websocketServer = createServer((request, response) => {
//     response.writeHead(404);
//     response.end();
// });
//
// // Bind it to port and start listening
// websocketServer.listen(WS_PORT, () => console.log(
//     `Websocket Server is now running on http://localhost:${WS_PORT}`
// ));

// const subscriptionsServer = new SubscriptionServer(
//     {
//         execute,
//         subscribe,
//         schema,
//     },
//     {
//         server: websocketServer
//     }
// );
var app = express();
app.options('/graphql', cors());
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json());
app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Credentials', 'true');
    res.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT,DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');
    res.setHeader('Cache-Control', 'no-cache');
    next();
});
app.use("/graphql",authorizeUser, function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
    if (req.method === 'OPTIONS') {
        res.sendStatus(200);
    } else {
        next();
    }
});
app.use("/graphiql",graphqlHTTP({
    graphiql:true,
    schema:schema
}));
// app.use('/graphiql123', graphiqlExpress({
//     endpointURL: "/graphql",
//     subscriptionsEndpoint: `ws://localhost:5000/`
// }));
app.use('/graphql', graphqlHTTP((request) => ({
    schema: schema,
    rootValue: { session: request.session },
    context:request.headers
})));

const server = createServer(app);
server.listen(4002, ()=>{
  new SubscriptionServer({
    execute,
    subscribe,
    schema: schema
  },{
    server: server,
    path: '/subscriptions'
  });
});


// app.listen(4002,function (){
//     console.log('Running a GraphQL API server at localhost:4002/graphql');
// });
