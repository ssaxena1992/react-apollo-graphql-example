import jwt from 'jsonwebtoken';
import Authorities from '../model/rolesAndResponsibilities';
function authorizeUser(req,res,next) {
    if(req.body.operationName === "loginUser" || req.body.operationName === "createUser"){
       next();
    }else{
      let token = req.headers.authorization;
      console.log(token);
      if(token){
  			jwt.verify(token, 'secret', function(err,decoded){
  				if(err){
                    res.json({success:false, message:" token authentication failed"});
                    next(err);
  				}else{
                    Authorities.getAuthorityByUserType(decoded.userType).then(function(userAuthorities){
                      /*if(userAuthorities){
                        console.log(userAuthorities);
                        req.userAuthorities = userAuthorities;
                        next();
                      }else{
                        res.json({success: false, message: "User Type could not be found in the database"});
                      }*/
                      next();
                    });
  				}
  			});
  		}else{
  			res.json({success:false, message:'No token provided'});
  		}
    }
}

export default authorizeUser;
