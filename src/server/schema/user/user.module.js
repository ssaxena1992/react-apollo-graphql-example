const createUser = {
    queryName: 'createUser',
    moduleName: 'user',
    operation: 'C',
    isDataVerificationReq: 'false'
};

const loginUser = {
    queryName: 'loginUser',
    moduleName: 'user',
    operation: 'R',
    isDataVerificationReq: 'false'
};

const readUser = {
    queryName: 'readUser',
    moduleName: 'user',
    operation: 'R',
    isDataVerificationReq: 'false'
};

const updateUser = {
    queryName: 'updateUser',
    moduleName: 'user',
    operation: 'U',
    isDataVerificationReq: 'false'
};

const deleteUser = {
    queryName: 'deleteUser',
    moduleName: 'user',
    operation: 'D',
    isDataVerificationReq: 'false'
};

module.exports = {
    createUser : createUser,
    loginUser : loginUser,
    readUser : readUser,
    updateUser : updateUser,
    deleteUser : deleteUser
}


