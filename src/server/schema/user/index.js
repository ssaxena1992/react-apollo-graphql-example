import User from './user.schema';
import {createUser, loginUser, readUser, updateUser, deleteUser} from './user.module';

module.exports = {
    User: User,
    createUser : createUser,
    loginUser : loginUser,
    readUser : readUser,
    updateUser : updateUser,
    deleteUser : deleteUser
}
