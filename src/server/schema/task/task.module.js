const addTask = {
    queryName: 'addTask',
    moduleName: 'user',
    operation: 'C',
    isDataVerificationReq: 'false'
};

const multipleTodos = {
    queryName: 'multipleTodos',
    moduleName: 'user',
    operation: 'R',
    isDataVerificationReq: 'false'
};

const singleTodo = {
    queryName: 'singleTodo',
    moduleName: 'user',
    operation: 'R',
    isDataVerificationReq: 'false'
};

const updateTask = {
    queryName: 'updateTask',
    moduleName: 'user',
    operation: 'U',
    isDataVerificationReq: 'false'
};

const deleteTask = {
    queryName: 'deleteTask',
    moduleName: 'user',
    operation: 'D',
    isDataVerificationReq: 'false'
};

module.exports = {
    addTask : addTask,
    multipleTodos : multipleTodos,
    singleTodo : singleTodo,
    updateTask : updateTask,
    deleteTask : deleteTask
}


