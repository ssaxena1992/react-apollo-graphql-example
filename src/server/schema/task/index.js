import Task from './task.schema';
import {addTask, multipleTodos, singleTodo, updateTask, deleteTask} from './task.module';

module.exports = {
    Task: Task,
    addTask : addTask,
    multipleTodos : multipleTodos,
    singleTodo : singleTodo,
    updateTask : updateTask,
    deleteTask : deleteTask
};
