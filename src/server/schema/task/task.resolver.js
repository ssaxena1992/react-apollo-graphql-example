var {PubSub}  =  require('graphql-subscriptions');
const TASK_ADDED = 'addedTask';
import Tasks from '../../model/tasks.model';
import Users from '../../model/users.model';
import { authorities } from '../../authorities';
import {getLoggedinUserData} from '../../../server/common/jwt';
var pubsub = new PubSub();
const prepare = (o) => {
    o._id = o._id.toString()
    return o
};

const taskResolver = {
    Query: {
        multipleTodos: async () => {
            var data = (await Tasks.find({}).exec()).map(prepare);
            return data;
        },
        singleTodo: (root,{taskName}) => {

            var foundItems = new Promise((resolve, reject) => {
                //var projections = getProjection(fieldASTs);
                Tasks.find({taskName},(err, todos) => {
                    //console.log(err,todos);
                    err ? reject(err) : resolve(todos)
                }).exec()
            });

            return foundItems;

        }
    },
    todoType:{
        Author: async ({userId}) => {
            var data = (await Users.findOne({_id: ObjectId(userId)}).exec())
            return data;

        }
    },
    Mutation: {
        addTask: (root,params,context) => {
            var token = context.authorization;
            if(token){
                getLoggedinUserData(token, function (err,userData){
                    if(userData){
                        //console.log(err,userData);
                        params.newTodo.userId = userData._id;
                        var taskData = new Tasks(params.newTodo);
                        pubsub.publish(TASK_ADDED, taskData)

                        return taskData.save();
                    }

                });
            }else{
                res.json({success:false, message:'No token provided'});
            }


        },
        updateTask:(root,params)=>{
            return authorities('endUser').updateTaskById(params.id,params.updatedTodo);
        },
        deleteTask: (root,params) => {
            return authorities('endUser').removeTaskById(params.id);
        },
    },

    Subscription: {
        addedTask: {
            subscribe:() =>  pubsub.asyncIterator(TASK_ADDED),
            resolve: (payload) => {
                console.log('The payload is ::: ', payload);
                return payload;
            }
        },
    }
};

export default taskResolver;