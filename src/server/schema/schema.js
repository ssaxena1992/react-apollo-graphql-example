//Please add all the schema at this place for now
import User from './user/user.schema';
import Task from './task/task.schema';
import _ from 'lodash';
import {mergeStrings} from 'gql-merge'

// making it empty as, root queries can be added in this
let typeDefs = _.concat(User, Task);

typeDefs = mergeStrings(typeDefs);
export default typeDefs;