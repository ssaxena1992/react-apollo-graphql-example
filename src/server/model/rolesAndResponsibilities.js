import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const authoritiesSchema = new Schema({
  userType: {type: String, required: true},
  operations: [{
    name: {type: String, required:true},
    isAllowed: {type: Boolean, default:false}
  }]
}, {collection: 'authorities', timestamp: true});

authoritiesSchema.statics.getAuthorityByUserType = function(userType) {
  return this.findOne({userType:userType},{operations:1, _id:0}).exec();
}

module.exports = mongoose.model('Authorities', authoritiesSchema);
