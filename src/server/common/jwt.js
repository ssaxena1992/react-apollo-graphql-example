import jwt from 'jsonwebtoken';
import Users from "../model/users.model";
//const jwt = require('jsonwebtoken');


function generateToken(payload, expiresIn) {
  let token = jwt.sign(payload, 'secret', { expiresIn: expiresIn });
  return token;
}

async function authenticateToken(token) {
  if(token){
    let tokenVerificationResult;
    try{
      tokenVerificationResult = await jwt.verify(token,'secret');
    }
    catch(err){
      if(err){
        return({success: false, message: 'token authentication failed', payload:null});
      }
    }
    return ({success: false, message: 'token authentication successful', payload:tokenVerificationResult});
  }
}

 function getLoggedinUserData(token,cb){

  if(token){
      try{
          jwt.verify(token,'secret',async function (err,decoded){

              if(err){
                  return;
              }else{

                  var email = decoded.email;
                  var loggedinUserData = await Users.findOne({email: email});
                  //console.log(loggedinUserData);
                  if(loggedinUserData){
                      cb(err,loggedinUserData);
                  }

              }
          });
      }
      catch(err){
          if(err){
              cb(err,null);
          }
      }
  }
}


module.exports = {
  generateToken:generateToken,
  authenticateToken:authenticateToken,
    getLoggedinUserData:getLoggedinUserData
}
