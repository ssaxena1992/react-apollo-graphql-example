import bcrypt from 'bcrypt-nodejs';

function createPasswordHash(password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(10), null);
}

function validatePassword(userPassword, password) {
  return bcrypt.compareSync(password, userPassword);
}

module.exports = {
  createPasswordHash:createPasswordHash,
  validatePassword:validatePassword
}
